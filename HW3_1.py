#This functin is written in reference to the n-link inverted pendulum example online
#We can animate the n-link pendulum now

from sympy import symbols
from sympy.physics.mechanics import *
from sympy import Dummy, lambdify
from numpy import array, hstack, zeros, linspace, pi, ones
from numpy.linalg import solve
from scipy.integrate import odeint
from numpy import zeros, cos, sin, arange, around
from matplotlib import pyplot as plt
from matplotlib import animation

n=input('Please input the number of links:    ')
q = dynamicsymbols('q:' + str(n))  # Generalized coordinates
u = dynamicsymbols('u:' + str(n))  # Generalized speeds

m = symbols('m:' + str(n))         # Mass of each bob
l = symbols('l:' + str(n))             # Length of each link
g, t = symbols('g t')                  # Gravity and time

I = ReferenceFrame('I')                # Inertial reference frame
O = Point('O')                         # Origin point
O.set_vel(I, 0)                        # Origin's velocity is zero

frames = [I]                              # List to hold the n frames
points = [ ]                             # List to hold the n  points
particles = [ ]                         # List to hold the n  particles
forces = [ ] 							# List to hold the n applied forces,
kindiffs = [ ]          				# List to hold kinematic ODE's

for i in range(n):
    Bi = I.orientnew('B' + str(i), 'Axis', [q[i], I.z])   # Create a new frame
    Bi.set_ang_vel(I, u[i] * I.z)                         # Set angular velocity
    frames.append(Bi)										# Add it to the frames list
    if i == 0:
    	Pi=O.locatenew('P' + str(i), l[i] * Bi.x)
    	Pi.v2pt_theory(O, I, Bi) 
    else:
    	Pi = points[-1].locatenew('P' + str(i), l[i] * Bi.x)  # Create a new point
    	Pi.v2pt_theory(points[-1], I, Bi)                       # Set the velocity
    points.append(Pi)                                         # Add it to the points list
    Pai = Particle('Pa' + str(i), Pi, m[i])           # Create a new particle
    particles.append(Pai)                                     # Add it to the particles list

    forces.append((Pi, -m[i] * g * I.y))                  # Set the force applied at the point
        
    kindiffs.append(q[i].diff(t) - u[i])              # Define the kinematic ODE:  dq_i / dt - u_i = 0

kane = KanesMethod(I, q_ind=q, u_ind=u, kd_eqs=kindiffs) # Initialize the object
fr, frstar = kane.kanes_equations(forces, particles)     # Generate EoM's fr + frstar = 0
arm_length = 1. / n                          # The maximum length of the pendulum is 1 meter
bob_mass = 0.01 / n                          # The maximum mass of the bobs is 10 grams
parameters = [g]                       # Parameter definitions starting with gravity
parameter_vals = [9.81]            # Numerical values for the first

for i in range(n):                           # Then each mass and length
    parameters += [l[i], m[i]]          
    parameter_vals += [arm_length, bob_mass]

dynamic = q + u                                                # Make a list of the states
dummy_symbols = [Dummy() for i in dynamic]                     # Create a dummy symbol for each variable
dummy_dict = dict(zip(dynamic, dummy_symbols))                 
kindiff_dict = kane.kindiffdict()                              # Get the solved kinematical differential equations
M = kane.mass_matrix_full.subs(kindiff_dict).subs(dummy_dict)  # Substitute into the mass matrix 
F = kane.forcing_full.subs(kindiff_dict).subs(dummy_dict)      # Substitute into the forcing vector
M_func = lambdify(dummy_symbols + parameters, M)               # Create a callable function to evaluate the mass matrix 
F_func = lambdify(dummy_symbols + parameters, F) 

def diff_eqn(x, t, args):
    """Returns the derivatives of the states.

    Parameters
    ----------
    x : ndarray, shape(2 * (n1))
        The current state vector.
    t : float
        The current time.
    args : ndarray
        The constants.

    Returns
    -------
    dx : ndarray, shape(2 * (n))
        The derivative of the state.
    
    """
    # u = 0.0                              # The input force is always zero     
    arguments = hstack((x, args))     # States, input, and parameters
    dx = array(solve(M_func(*arguments), # Solving for the derivatives
        F_func(*arguments))).T[0]
    
    return dx

x0 = hstack(( 0, pi / 2 * ones(len(q) - 1),zeros(len(u)) )) # Initial conditions, q and u
t = linspace(0, 10, 1000)                                          # Time vector
y = odeint(diff_eqn, x0, t, args=(parameter_vals,)) 

def animate_pendulum(t, states, length, filename=None):
    """Animates the n-pendulum and optionally saves it to file.

    Parameters
    ----------
    t : ndarray, shape(m)
        Time array.
    states: ndarray, shape(m,p)
        State time history.
    length: float
        The length of the pendulum links.
    filename: string or None, optional
        If true a movie file will be saved of the animation. This may take some time.

    Returns
    -------
    fig : matplotlib.Figure
        The figure.
    anim : matplotlib.FuncAnimation
        The animation.

    """
    # the number of pendulum bobs
    numpoints = states.shape[1] / 2

    # first set up the figure, the axis, and the plot elements we want to animate
    fig = plt.figure()
    
    # create the axes
    ax = plt.axes(xlim=(-1.1, 1.1), ylim=(-1.1, 1.1), aspect='equal')
    
    # display the current time
    time_text = ax.text(0.04, 0.9, '', transform=ax.transAxes)
    
    # blank line for the pendulum
    line, = ax.plot([], [], lw=2, marker='o', markersize=6)

    # initialization function: plot the background of each frame
    def init():
        time_text.set_text('')
        line.set_data([], [])
        return time_text, line,

    # animation function: update the objects
    def animate(i):
        time_text.set_text('time = {:2.2f}'.format(t[i]))
        x = zeros((numpoints+1))
        y = zeros((numpoints+1))
        for j in arange(1, numpoints+1):
            x[j] = x[j - 1] + length * cos(states[i, j-1])
            y[j] = y[j - 1] + length * sin(states[i, j-1])
        line.set_data(x, y)
        return time_text, line,

    # call the animator function
    anim = animation.FuncAnimation(fig, animate, frames=len(t), init_func=init,
            interval=t[-1] / len(t) * 1000, blit=False, repeat=False)
    plt.show()

animate_pendulum(t, y, arm_length, filename="open-loop.mp4")