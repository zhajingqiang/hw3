# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c
# @editor: gray_thomas

from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

class Pendulum_Simulation:
    def __init__(self):
        self.G =  9.8 # acceleration due to gravity, in m/s^2
        self.L1 = 1.0 # length of pendulum 1 in m
        self.L2 = 1.0 # length of pendulum 2 in m
        self.M1 = 1.0 # mass of pendulum 1 in kg
        self.M2 = 1.0 # mass of pendulum 2 in kg
        self.T0 = 0 # seconds
        self.Tf = 20 # seconds
        self.dt = 0.05 # seconds
        # self.init()

    def derivs(self, state, t):
        G=self.G
        L1=self.L1
        L2=self.L2
        M1=self.M1
        M2=self.M2
        dydx = np.zeros_like(state)
        dydx[0] = state[1]

        del_ = state[2]-state[0]
        den1 = (M1+M2)*L1 - M2*L1*cos(del_)*cos(del_)
        dydx[1] = (M2*L1*state[1]*state[1]*sin(del_)*cos(del_)
                 + M2*G*sin(state[2])*cos(del_) + M2*L2*state[3]*state[3]*sin(del_)
                 - (M1+M2)*G*sin(state[0]))/den1

        dydx[2] = state[3]

        den2 = (L2/L1)*den1
        dydx[3] = (-M2*L2*state[3]*state[3]*sin(del_)*cos(del_)
                 + (M1+M2)*G*sin(state[0])*cos(del_)
                 - (M1+M2)*L1*state[1]*state[1]*sin(del_)
                 - (M1+M2)*G*sin(state[2]))/den2
        dydx[4]=1
        return dydx

    def simulate(self): 
        G=self.G
        L1=self.L1
        L2=self.L2
        M1=self.M1
        M2=self.M2
        # create a time array from 0..100 sampled at 0.1 second steps
        
        self.t = np.arange(self.T0, self.Tf, self.dt)

        # th1 and th2 are the initial angles (degrees)
        # w10 and w20 are the initial angular velocities (degrees per second)
        self.th1 = 120.0
        self.w1 = 0.0
        self.th2 = -10.0
        self.w2 = 0.0

        self.rad = pi/180

        # initial state
        self.state = np.array([self.th1, self.w1, self.th2, self.w2, self.t[0]])*pi/180.

        # integrate your ODE using scipy.integrate.
        self.y = integrate.odeint(self.derivs, self.state, self.t)

        self.x1 = L1*sin(self.y[:,[0]])
        self.y1 = -L1*cos(self.y[:,[0]])

        self.x2 = L2*sin(self.y[:,[2]]) + self.x1
        self.y2 = -L2*cos(self.y[:,[2]]) + self.y1
        self.data = np.concatenate((self.y,self.x1,self.x2,self.y1,self.y2),axis=1)
        self.data_indexes={"theta 1":0,"theta dot 1":2,"theta 2":1,"theta dot 2":3,"time":4,"point 1 x":5,"point 2 x":6,"point 1 y":7, "point 2 y":8}

    def get_data(self):
        return self.data
    def get_data_indexes(self):
        return self.data_indexes

    def init(self):
        self.line, = self.ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def init_animation(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def animate(self,i):
        self.thisx = [0, self.x1[i], self.x2[i]]
        self.thisy = [0, self.y1[i], self.y2[i]]

        self.line.set_data(self.thisx, self.thisy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text

    def begin_animation(self):
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.y)),interval=50, blit=False, init_func=self.init_animation)
        print "RUNNING Animation"

    def generate_figure(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
        self.ax.grid()
        return self.fig

#ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
def main():
    sim = Pendulum_Simulation()
    sim.generate_figure()
    sim.init()
    sim.simulate()
    sim.G=-9.8
    sim.M1=2
    sim.simulate()
    sim.begin_animation()
    plt.show()

if __name__ == "__main__":
    main()
